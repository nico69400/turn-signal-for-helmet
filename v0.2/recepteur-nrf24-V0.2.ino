/*
 * Basé sur le code de Passion-Électronique
 * v0.2 - 01/11/2021
*/
#include <SPI.h>
#include <RF24.h>
#include <Adafruit_NeoPixel.h>

#define pinCE   7             // On associe la broche "CE" du NRF24L01 à la sortie digitale D7 de l'arduino
#define pinCSN  8             // On associe la broche "CSN" du NRF24L01 à la sortie digitale D8 de l'arduino
#define tunnel  "PIPE2"       // On définit le "nom de tunnel" (5 caractères) à travers lequel on va recevoir les données de l'émetteur
#define tunnel2 "PIPE1"     // On définit un second "nom de tunnel" (5 caractères), pour pouvoir recevoir des données de l'autre NRF24
#define PIN      4          // Le ruban WS2812B est connecté à la sortie D4
#define NUMPIXELS 5         // Nombre de pixels

RF24 radio(pinCE, pinCSN);    // Instanciation du NRF24L01
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

const byte adresse[][6] = {tunnel, tunnel2};       // Mise au format "byte array" du nom du tunnel
char message[32];                     // Avec cette librairie, on est "limité" à 32 caractères par message

void setup() {
  // Initialisation du port série (pour afficher les infos reçues, sur le "Moniteur Série" de l'IDE Arduino)
  Serial.begin(9600);
  Serial.println("Récepteur NRF24L01");
  Serial.println("");
  pixels.begin();
  
  // Partie NRF24
  
  radio.begin();                      // Initialisation du module NRF24
  radio.openWritingPipe(adresse[0]);      // Ouverture du "tunnel1" en ÉCRITURE
  radio.openReadingPipe(1, adresse[1]);   // Ouverture du "tunnel2" en LECTURE
  radio.setPALevel(RF24_PA_MIN);      // Sélection d'un niveau "MINIMAL" pour communiquer (pas besoin d'une forte puissance, pour nos essais)
  radio.startListening();             // Démarrage de l'écoute du NRF24 (signifiant qu'on va recevoir, et non émettre quoi que ce soit, ici)
}

void loop() {
  pixels.setBrightness(100);
    for (int i = 0; i < 5; i++){
      pixels.setPixelColor(i, 255, 0, 0); //Boucle d'affichage des leds en rouge clignotants
    } 
    pixels.show();
    
  // On vérifie à chaque boucle si un message est arrivé
  if (radio.available()) {
    //Serial.println("debut ecoute...");
    radio.read(&message, sizeof(message)); // Si un message vient d'arriver, on le charge dans la variable "message"
    //Serial.print("Message reçu : "); 
    //Serial.println(message);          // … et on l'affiche sur le port série !
    if ( 'R' == message[0] ){           // Le message envoyé par l'emetteur est -il "R"?
      pixels.clear();
      pixels.setBrightness(100);
      //int bright = 20;
      //int i = 4;

      while ('S' != message[0]){    //On exécutera la boucle tant que l'on ne reçoit pas de l'emetteur de message "S"
        for (int i = 0; i < 5; i++){
          pixels.setBrightness(100);
          pixels.setPixelColor(i, 0, 0, 0); //passage des pixels à VIDE
        }
        pixels.show();
        delay(100);

        for (int i = 4; i >= 0; i = i - 1){
          pixels.setPixelColor(i, 255, 255, 0);  //Passage des pixels au jaune
          pixels.show();  //ici on affiche pixel par pixel (le pixels.show est ds la boucle
          delay(100);
        } 
      radio.read(&message, sizeof(message));  //En écoute pour savoir si on reçoit le message "S"
      pixels.clear();
      }
    }
    else{
      //Serial.println("pas R");
    }
    radio.startListening();

   if ( 'L' == message[0] ){
      pixels.clear();
      while ('S' != message[0]){
        for (int i = 0; i < 5; i++){
          pixels.setBrightness(100);
          pixels.setPixelColor(i, 0, 0, 0);
        }
        pixels.show();
        delay(100); 
        //Serial.println("je tourne: ");
        for (int i = 0; i < 5; i++){
          pixels.setBrightness(100);
          pixels.setPixelColor(i, 255, 255, 0); 
          pixels.show();
          delay(100);
        } 
      //Serial.println("light - yello - left");
      pixels.show();
      radio.read(&message, sizeof(message));
      pixels.clear();
      }
    }
    else{
      //Serial.println("pas L");
    }
    radio.startListening();
  }

    delay(100);  
    for (int j = 0; j < 5; j++){
      pixels.setPixelColor(j, 0, 0, 0); //passage des pixels à VIDE pour le clignotement.
    } 
    //Serial.println("light - red - blink");
    pixels.show();
    delay(100);
}
