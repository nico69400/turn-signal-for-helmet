/*
 * Basé sur le code de Passion-Électronique
 * v0.2 - 01/11/2021
*/
#include <SPI.h>
#include <RF24.h>

#define pinCE   7             // On associe la broche "CE" du NRF24L01 à la sortie digitale D7 de l'arduino
#define pinCSN  8             // On associe la broche "CSN" du NRF24L01 à la sortie digitale D8 de l'arduino
#define tunnel  "PIPE1"       // On définit un "nom de tunnel" (5 caractères), pour pouvoir communiquer d'un NRF24 à l'autre
#define tunnel2 "PIPE2"  

RF24 radio(pinCE, pinCSN);    // Instanciation du NRF24L01

const byte adresse[][6] = {tunnel, tunnel2};               // Mise au format "byte array" du nom du tunnel
const char message[] = "Hello World !!!";     // Message à transmettre à l'autre NRF24 (32 caractères maxi, avec cette librairie)
char message1[32];                     // Avec cette librairie, on est "limité" à 32 caractères par message
char message2[32];                     // Avec cette librairie, on est "limité" à 32 caractères par message

const int buttonPin2 = 2;  //Branchement du bouton Right sortie D2
const int buttonPin3 = 3;   //Branchement du bouton Left sortie D3
const int buttonPin4 = 4;   //Branchement du bouton Stop sortie D4
int buttonState2 = 0; 
int buttonState3 = 0; 
int buttonState4 = 0; 
bool ack_radio = false;

void setup() {
  Serial.begin(9600);
  pinMode(buttonPin2, INPUT_PULLUP); 
  pinMode(buttonPin3, INPUT_PULLUP); 
  pinMode(buttonPin4, INPUT_PULLUP); 
  Serial.println("Emetteur NRF24L01");
  Serial.println("");
  radio.begin();                      // Initialisation du module NRF24
  radio.openWritingPipe(adresse[0]);     // Ouverture du tunnel en ÉCRITURE, avec le "nom" qu'on lui a donné
  radio.openReadingPipe(1, adresse[1]);   // Ouverture du "tunnel2" en LECTURE
  radio.setPALevel(RF24_PA_MIN);      // Sélection d'un niveau "MINIMAL" pour communiquer (pas besoin d'une forte puissance, pour nos essais)
  radio.stopListening();              // Arrêt de l'écoute du NRF24 (signifiant qu'on va émettre, et non recevoir, ici)
}

void loop() {
  int x = 0;
  buttonState2 = digitalRead(buttonPin2);
  buttonState3 = digitalRead(buttonPin3);
  buttonState4 = digitalRead(buttonPin4);
  if (buttonState2 == HIGH) {
      // turn LED off:
      Serial.println("Right *");
      char message1;
      message1 = "R";    // Dans la limite de 32 octets (32 caractères, ici)
      radio.stopListening();
      radio.write("R", sizeof("R"));  
  }
  if (buttonState3 == HIGH) {
      // turn LED off:
      //Serial.println("Left *");
      char message1;
      message1 = "L";    // Dans la limite de 32 octets (32 caractères, ici)
      radio.stopListening();
      radio.write("L", sizeof("L"));  
  }
  
  if (buttonState4 == HIGH) {
      // turn LED off:
      //Serial.println("Stop *");
      char message1;
      message1 = "S";    // Dans la limite de 32 octets (32 caractères, ici)
      radio.stopListening();
      radio.write("S", sizeof("S"));  
  }
  
  if (radio.available()) {
  radio.startListening();
  radio.read(&message2, sizeof(message2));
  //Serial.println(message2);
  radio.stopListening();
  }
   delay(200);                                // … toutes les secondes ! 
}
