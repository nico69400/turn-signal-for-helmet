/**
   TURN SIGNAL FOR HELMET - Helmet signal
   V1.2
   @nico69400
   05/12/2021


   Use MATRIX 64 WS2812B connected on pin 4


*/


#include <esp_now.h>
#include <WiFi.h>
#include <Adafruit_NeoPixel.h>

#define CHANNEL 1
#define PIN      4          // Le ruban WS2812B est connecté à la sortie D4
#define NUMPIXELS 64         // Nombre de pixels
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

bool right = false;
bool left = false;
bool stopA = false;
int POS;
int valPixel1 = 0;
int valPixel2 = 0;
int valPixel3 = 0;
// int RedMatrix[64] = {
//   0,0,1,1,1,1,0,0,
//   0,0,1,1,1,1,0,0,
//   0,0,1,1,1,1,0,0,
//   0,0,1,1,1,1,0,0,
//   0,0,1,1,1,1,0,0,
//   0,0,1,1,1,1,0,0,
//   0,0,1,1,1,1,0,0,
//   0,0,1,1,1,1,0,0
//   };

int RedMatrix[64] = {
  1,1,1,1,1,1,1,1,
  1,0,0,0,0,0,0,1,
  1,0,0,0,0,0,0,1,
  1,0,0,0,0,0,0,1,
  1,0,0,0,0,0,0,1,
  1,0,0,0,0,0,0,1,
  1,0,0,0,0,0,0,1,
  1,1,1,1,1,1,1,1
  };

int LeftMatrix1[64] = {
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  2,0,0,0,0,0,0,0,
  0,2,0,0,0,0,0,0,
  2,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0
  };

int LeftMatrix2[64] = {
  2,0,0,0,0,0,0,0,
  0,2,0,0,0,0,0,0,
  0,0,2,0,0,0,0,0,
  0,0,0,2,0,0,0,0,
  0,0,2,0,0,0,0,0,
  0,2,0,0,0,0,0,0,
  2,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0
  };

int LeftMatrix3[64] = {
  0,0,2,0,0,0,0,0,
  0,0,0,2,0,0,0,0,
  0,0,0,0,2,0,0,0,
  0,0,0,0,0,2,0,0,
  0,0,0,0,2,0,0,0,
  0,0,0,2,0,0,0,0,
  0,0,2,0,0,0,0,0,
  0,0,0,0,0,0,0,0
  };

int LeftMatrix4[64] = {
  0,0,0,0,2,0,0,0,
  0,0,0,0,0,2,0,0,
  0,0,0,0,0,0,2,0,
  0,0,0,0,0,0,0,2,
  0,0,0,0,0,0,2,0,
  0,0,0,0,0,2,0,0,
  0,0,0,0,2,0,0,0,
  0,0,0,0,0,0,0,0
  };

int RightMatrix1[64] = {
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,2,
  0,0,0,0,0,0,2,0,
  0,0,0,0,0,0,0,2,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0
  };

int RightMatrix2[64] = {
  0,0,0,0,0,0,0,2,
  0,0,0,0,0,0,2,0,
  0,0,0,0,0,2,0,0,
  0,0,0,0,2,0,0,0,
  0,0,0,0,0,2,0,0,
  0,0,0,0,0,0,2,0,
  0,0,0,0,0,0,0,2,
  0,0,0,0,0,0,0,0
  };

int RightMatrix3[64] = {
  0,0,0,0,0,2,0,0,
  0,0,0,0,2,0,0,0,
  0,0,0,2,0,0,0,0,
  0,0,2,0,0,0,0,0,
  0,0,0,2,0,0,0,0,
  0,0,0,0,2,0,0,0,
  0,0,0,0,0,2,0,0,
  0,0,0,0,0,0,0,0
  };

int RightMatrix4[64] = {
  0,0,0,2,0,0,0,0,
  0,0,2,0,0,0,0,0,
  0,2,0,0,0,0,0,0,
  2,0,0,0,0,0,0,0,
  0,2,0,0,0,0,0,0,
  0,0,2,0,0,0,0,0,
  0,0,0,2,0,0,0,0,
  0,0,0,0,0,0,0,0
  };



// Init ESP Now with fallback
void InitESPNow() {
  WiFi.disconnect();
  if (esp_now_init() == ESP_OK) {
    Serial.println("ESPNow Init Success");
  }
  else {
    Serial.println("ESPNow Init Failed");
    ESP.restart();
  }
}

// config AP SSID
void configDeviceAP() {
  const char *SSID = "Slave_1";
  bool result = WiFi.softAP(SSID, "Slave_1_Password", CHANNEL, 0);
  if (!result) {
    Serial.println("AP Config failed.");
  } else {
    Serial.println("AP Config Success. Broadcasting with AP: " + String(SSID));
  }
}

void setup() {
  Serial.begin(115200);
  Serial.println("Xiaomi Receiver Light");
  //Set device in AP mode to begin with
  WiFi.mode(WIFI_AP);
  // configure device AP mode
  configDeviceAP();
  // This is the mac address of the Slave in AP Mode
  Serial.print("AP MAC: "); Serial.println(WiFi.softAPmacAddress());
  // Init ESPNow with a fallback logic
  InitESPNow();
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info.
  // esp_now_register_recv_cb(OnDataRecv);

}

// callback when data is recv from Master
void OnDataRecv(const uint8_t *mac_addr, const uint8_t *data, int data_len) {
  char macStr[18];
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  POS = *data;
  Serial.println("");
}

void blinker(int data){

     if ( 1 == data || right ){
        for (int i = 0; i < 64; i++){
          pixels.setBrightness(100);
          pixels.setPixelColor(i, 0, 0, 0);
        }
        pixels.show();
        delay(200);
        for (int i = 0; i < 64; i++){
          if ( RightMatrix1[i] == 0 ){
            valPixel1 = 0;
            valPixel2 = 0;
            }
          else if ( RightMatrix1[i] == 1 ){
            valPixel1 = 255;
            valPixel2 = 0;
          }
          else{
            valPixel1 = 255;
            valPixel2 = 255;
          }
          pixels.setPixelColor(i, valPixel1, valPixel2, valPixel3);
           
        } 
        pixels.show();
        delay(200);
        for (int i = 0; i < 64; i++){
          if ( RightMatrix2[i] == 0 ){
            valPixel1 = 0;
            valPixel2 = 0;
            }
          else if ( RightMatrix2[i] == 1 ){
            valPixel1 = 255;
            valPixel2 = 0;
          }
          else{
            valPixel1 = 255;
            valPixel2 = 255;
          }
          pixels.setPixelColor(i, valPixel1, valPixel2, valPixel3);
           
        } 
        pixels.show();
        delay(200);
        for (int i = 0; i < 64; i++){
          if ( RightMatrix3[i] == 0 ){
            valPixel1 = 0;
            valPixel2 = 0;
            }
          else if ( RightMatrix3[i] == 1 ){
            valPixel1 = 255;
            valPixel2 = 0;
          }
          else{
            valPixel1 = 255;
            valPixel2 = 255;
          }
          pixels.setPixelColor(i, valPixel1, valPixel2, valPixel3);
           
        } 
        pixels.show();
        delay(200);
        for (int i = 0; i < 64; i++){
          if ( RightMatrix4[i] == 0 ){
            valPixel1 = 0;
            valPixel2 = 0;
            }
          else if ( RightMatrix4[i] == 1 ){
            valPixel1 = 255;
            valPixel2 = 0;
          }
          else{
            valPixel1 = 255;
            valPixel2 = 255;
          }
          pixels.setPixelColor(i, valPixel1, valPixel2, valPixel3);
           
        } 
        pixels.show();
        delay(200);

        for (int j=0; j < 64; j++){
              pixels.setPixelColor(j, 0, 0, 0);
            } 
          pixels.show();
          delay(200);
          bool right = true;
          bool left = false;
          bool stopA = false;
      delay(100);
  data = 1;
    }
    else{
      //Serial.println("pas L");
    }


     if ( 2 == data || left ){
        for (int i = 0; i < 64; i++){
          pixels.setBrightness(100);
          pixels.setPixelColor(i, 0, 0, 0);
        }
        pixels.show();
        delay(200); 
        for (int i = 0; i < 64; i++){
          if ( LeftMatrix1[i] == 0 ){
            valPixel1 = 0;
            valPixel2 = 0;
            }
          else if ( LeftMatrix1[i] == 1 ){
            valPixel1 = 255;
            valPixel2 = 0;
          }
          else{
            valPixel1 = 255;
            valPixel2 = 255;
          }
          pixels.setPixelColor(i, valPixel1, valPixel2, valPixel3);
           
        } 
        pixels.show();
        delay(200);
                for (int i = 0; i < 64; i++){
          if ( LeftMatrix2[i] == 0 ){
            valPixel1 = 0;
            valPixel2 = 0;
            }
          else if ( LeftMatrix2[i] == 1 ){
            valPixel1 = 255;
            valPixel2 = 0;
          }
          else{
            valPixel1 = 255;
            valPixel2 = 255;
          }
          pixels.setPixelColor(i, valPixel1, valPixel2, valPixel3);
           
        } 
        pixels.show();
        delay(200);
                for (int i = 0; i < 64; i++){
          if ( LeftMatrix3[i] == 0 ){
            valPixel1 = 0;
            valPixel2 = 0;
            }
          else if ( LeftMatrix3[i] == 1 ){
            valPixel1 = 255;
            valPixel2 = 0;
          }
          else{
            valPixel1 = 255;
            valPixel2 = 255;
          }
          pixels.setPixelColor(i, valPixel1, valPixel2, valPixel3);
           
        } 
        pixels.show();
        delay(200);
                for (int i = 0; i < 64; i++){
          if ( LeftMatrix4[i] == 0 ){
            valPixel1 = 0;
            valPixel2 = 0;
            }
          else if ( LeftMatrix4[i] == 1 ){
            valPixel1 = 255;
            valPixel2 = 0;
          }
          else{
            valPixel1 = 255;
            valPixel2 = 255;
          }
          pixels.setPixelColor(i, valPixel1, valPixel2, valPixel3);
           
        } 
        pixels.show();
        delay(200);

        for (int j=0; j < 64; j++){
              pixels.setPixelColor(j, 0, 0, 0);
            } 
          pixels.show();
          delay(200);
          bool right = false;
          bool left = true;
          bool stopA = false;
      delay(100);
  data = 2;
    }
    else{
      //Serial.println("pas L");
    }

    if ( 3 == data || stopA){
      pixels.setBrightness(65);
      for (int i = 0; i < 64; i++){
        if ( RedMatrix[i] == 0 ){
          valPixel1 = 0;
          valPixel2 = 0;
        }
        else {
          valPixel1 = 255;
          valPixel2 = 0;
        }
            pixels.setPixelColor(i, valPixel1, valPixel2, valPixel3); //Boucle d'affichage des leds en rouge clignotants
            Serial.print("Valeur: ");
            Serial.println(RedMatrix[i]);
        } 
        pixels.show();
         delay(200);
         for (int j = 0; j < 64; j++){
           pixels.setPixelColor(j, 0, 0, 0); //passage des pixels à VIDE pour le clignotement.
         } 
         pixels.show();
        bool right = false;
        bool left = false;
        bool stopA = true;
        delay(200); 
        data = 3;  
        Serial.println("Nouveau tour");     
    }
  
}

void loop() {
//  int data = esp_now_register_recv_cb(OnDataRecv);
  esp_now_register_recv_cb(OnDataRecv);
  if (POS == 0){
     POS = 3;
  }
 blinker(POS);
}
