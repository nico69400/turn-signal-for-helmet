/**
   TURN SIGNAL FOR HELMET - Helmet signal
   V1.0
   @nico69400
   05/12/2021


   Use 8 WS2812B connected on pin 4


*/

#include <esp_now.h>
#include <WiFi.h>
#include <Adafruit_NeoPixel.h>

#define CHANNEL 1
#define PIN      4          // Le ruban WS2812B est connecté à la sortie 4
#define NUMPIXELS 11         // Nombre de pixels
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

bool right = false;
bool left = false;
bool stopA = false;
int POS;


// Init ESP Now with fallback
void InitESPNow() {
  WiFi.disconnect();
  if (esp_now_init() == ESP_OK) {
    Serial.println("ESPNow Init Success");
  }
  else {
    Serial.println("ESPNow Init Failed");
    // Retry InitESPNow, add a counte and then restart?
    // InitESPNow();
    // or Simply Restart
    ESP.restart();
  }
}

// config AP SSID
void configDeviceAP() {
  const char *SSID = "Slave_Helmet";
  bool result = WiFi.softAP(SSID, "Slave_1_Password", CHANNEL, 0);
  if (!result) {
    Serial.println("AP Config failed.");
  } else {
    Serial.println("AP Config Success. Broadcasting with AP: " + String(SSID));
  }
}

void setup() {
  Serial.begin(115200);
  Serial.println("Helmet Receiver Light");
  //Set device in AP mode to begin with
  WiFi.mode(WIFI_AP);
  // configure device AP mode
  configDeviceAP();
  // This is the mac address of the Slave in AP Mode
  Serial.print("AP MAC: "); Serial.println(WiFi.softAPmacAddress());
  // Init ESPNow with a fallback logic
  InitESPNow();
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info.
  // esp_now_register_recv_cb(OnDataRecv);

}

// callback when data is recv from Master
void OnDataRecv(const uint8_t *mac_addr, const uint8_t *data, int data_len) {
  char macStr[18];
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
//  Serial.print("Last Packet Recv from: "); Serial.println(macStr);
//  Serial.print("Last Packet Recv Data: "); Serial.println(*data);
  POS = *data;
  Serial.println("");
}

void blinker(int data){
    if ( 1 == data || right ){
      // Le message envoyé par l'emetteur est -il "R"?
      pixels.setBrightness(100);
        for (int i = 0; i < 3; i++){
          pixels.setBrightness(100);
          pixels.setPixelColor(i, 0, 0, 0); //passage des pixels à VIDE
        }
        pixels.show();
        delay(100);
          for ( int i = 0; i <3; i++){
            pixels.setPixelColor(i, 255, 255, 0);  //Passage des pixels au jaune
            for (int j=3; j < 11; j++){
              //pixels.setPixelColor(j, 255, 0, 0);  //Passage des pixels au rouge
              pixels.setPixelColor(j, 0, 0, 0);
            }
            //pixels.show();
            delay(100);
          }
          pixels.show();
          bool right = true;
          bool left = false;
          bool stopA = false;
      delay(100);
  data = 1;
    }
    else{
      //Serial.println("pas R");
    }
     if ( 2 == data || left ){
        for (int i = 8; i < 11; i++){
          pixels.setBrightness(100);
          pixels.setPixelColor(i, 0, 0, 0);
        }
        pixels.show();
        delay(100); 
        //Serial.println("je tourne: ");
        for (int i = 8; i < 11; i++){
          pixels.setBrightness(100);
          pixels.setPixelColor(i, 255, 255, 0);
          for (int j=0; j < 8; j++){
              pixels.setPixelColor(j, 0, 0, 0);
            } 
          //pixels.show();
          delay(100);
        } 
      //Serial.println("light - yellow - left");
      pixels.show();
          bool right = false;
          bool left = true;
          bool stopA = false;
      //radio.read(&message, sizeof(message));
      //pixels.clear();
      delay(100);
//      esp_now_register_recv_cb(OnDataRecv);
//      }
  data = 2;
    }
    else{
      //Serial.println("pas L");
    }
//    radio.startListening();
 //}
  if ( 3 == data || stopA){
      //pixels.clear();
      
        pixels.setBrightness(100);
        for (int i = 3; i < 8; i++){
            pixels.setPixelColor(i, 255, 0, 0); //Boucle d'affichage des leds en rouge clignotants
//            Serial.println("en rouge car stop");
        } 
        pixels.show();
        delay(100);
        for (int j = 0; j < 11; j++){
          pixels.setPixelColor(j, 0, 0, 0); //passage des pixels à VIDE pour le clignotement.
//          Serial.println("en rien car stop");
        } 
         //Serial.println("light - red - blink");
        pixels.show();
        bool right = false;
        bool left = false;
        bool stopA = true;
        delay(100); 
   data = 3;
  }
  
}

void loop() {
  esp_now_register_recv_cb(OnDataRecv);
//  Serial.print("je lis la variable message1 ");  Serial.println(POS);
  if (POS == 0){
     POS = 3;
  }
 blinker(POS);
}
